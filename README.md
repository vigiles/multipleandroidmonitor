管微Server端。学习UiAutomator2测试
====
使用Java在AndroidStudio上开发的群，控软件。
====
同上让多个手机启动程序加好友，聊天。
免root。

master模块运行TestMain的main方法。
====
<br/>
控制端。核心是依靠adb，结合rxjava实现同时给多个手机发送指令。手机端测试器app收到指令后进行响应。
<br/>
资源目录里需要这几个文件：<br/>

 friend.txt                        微==信号列表<br/>
 gimmick.txt                       话术，使用<a href="https://gitee.com/vigiles/gimmickmaker" target="_blank"> https://gitee.com/vigiles/gimmickmaker </a>工具制作<br/>
 word.txt                          分词<br/>
 com.tencent.mm_1570e359_6.7.3.apk 微-信app。可以用别的app测试<br/>
 app-debug-androidTest.apk         测试器apk。这个要在<a href="https://gitee.com/vigiles/monitorclient" target="_blank"> https://gitee.com/vigiles/monitorclient </a>项目开发<br/>
 app-debug.apk                     宿主apk，壳子。同上，在monitorclient项目里默认代码生成。<br/>
<br/>
<br/>

![UI](mam.jpg)

<br/>

![UI](mam2.jpg)

<hr/>
