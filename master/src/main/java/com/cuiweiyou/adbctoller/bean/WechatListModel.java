package com.cuiweiyou.adbctoller.bean;

import java.util.List;

import javax.swing.AbstractListModel;

/**
 * www.gaohaiyan.com
 */
public class WechatListModel extends AbstractListModel<String> {
    List<String> friendList;

    public WechatListModel(List<String> friendList) {
        this.friendList = friendList;
    }

    @Override
    public int getSize() {
        return null == friendList ? 0 : friendList.size();
    }

    @Override
    public String getElementAt(int i) {
        return friendList.get(i);
    }
}
