package com.cuiweiyou.adbctoller.bean;

/**
 * www.gaohaiyan.com
 */
public class WechatNumberModel {
	private String wxid;

	public WechatNumberModel() {
	}

	public WechatNumberModel(String wxid) {
		this.wxid = wxid;
	}

	public String getWxid() {
		return wxid;
	}

	public void setWxid(String wxid) {
		this.wxid = wxid;
	}

	@Override
	public String toString() {
		return "WechatNumberModel{" + "wxid='" + wxid + '}';
	}
}
