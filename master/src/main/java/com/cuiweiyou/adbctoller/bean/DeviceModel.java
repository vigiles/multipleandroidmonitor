package com.cuiweiyou.adbctoller.bean;

/**
 * www.gaohaiyan.com
 */
public class DeviceModel {
    String sn;                     // 串号
    String brand;                  // 厂商
    String model;                  // 型号
    String wxVersion = "_";        // 微信版本
    String androidVersion = "_";   // Android版本
    String apiVersion = "_";       // API版本
    String brandSystemVersion = "_"; // 厂商系统的版本，华为：ro.build.version.emui

    public DeviceModel() {
    }

    /**
     * @param sn                 串号
     * @param brand              厂商
     * @param model              型号
     * @param wxVersion          微信版本
     * @param androidVersion     Android版本
     * @param apiVersion         API版本
     * @param brandSystemVersion 厂商系统的版本，华为：ro.build.version.emui
     */
    public DeviceModel(String sn, String brand, String model, String wxVersion, String androidVersion, String apiVersion, String brandSystemVersion) {
        this.sn = sn;
        this.brand = brand;
        this.model = model;
        this.wxVersion = wxVersion;
        this.androidVersion = androidVersion;
        this.apiVersion = apiVersion;
        this.brandSystemVersion = brandSystemVersion;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getWxVersion() {
        return wxVersion;
    }

    public void setWxVersion(String wxVersion) {
        this.wxVersion = wxVersion;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getBrandSystemVersion() {
        return brandSystemVersion;
    }

    public void setBrandSystemVersion(String brandSystemVersion) {
        this.brandSystemVersion = brandSystemVersion;
    }

    @Override
    public String toString() {
        return "DeviceModel{" +
                "sn='" + sn + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", wxVersion='" + wxVersion + '\'' +
                ", androidVersion='" + androidVersion + '\'' +
                ", apiVersion='" + apiVersion + '\'' +
                ", brandSystemVersion='" + brandSystemVersion + '\'' +
                '}';
    }
}
