package com.cuiweiyou.adbctoller.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * www.gaohaiyan.com
 */
public class DateUtil {
    static DateFormat df = new SimpleDateFormat("yy-MM-dd-HH_mm_ss");

    private DateUtil() {
    }

    public static String getNowYYMMDDHHmmss() {
        String yyMMddhhmmss = df.format(new Date());
        return yyMMddhhmmss;
    }
}
