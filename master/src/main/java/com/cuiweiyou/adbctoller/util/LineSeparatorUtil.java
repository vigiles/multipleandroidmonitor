package com.cuiweiyou.adbctoller.util;

import com.cuiweiyou.adbctoller.bean.DeviceModel;

import java.util.ArrayList;
import java.util.List;

public class LineSeparatorUtil {
    private LineSeparatorUtil() {
    }

    public static List<String> transformStringListToListLog(String tip, List<String> list) {
        List<String> result = new ArrayList<>();
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                String line = list.get(i);
                result.add(DateUtil.getNowYYMMDDHHmmss() + "_" + tip + "：" + line);
            }
        }
        return result;
    }

    public static List<String> transformDeviceToListLog(String tip, List<DeviceModel> list) {
        List<String> result = new ArrayList<>();
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                DeviceModel device = list.get(i);
                result.add(DateUtil.getNowYYMMDDHHmmss() + "_" + tip + "：" + device.toString());
            }
        }
        return result;
    }

    public static List<String> transformStrigToListLog(String tip, String line) {
        List<String> result = new ArrayList<>();
        if (null != line && line.length() > 0) {
            String[] split = line.split(System.getProperty("line.separator"));
            for (int i = 0; i < split.length; i++) {
                result.add(DateUtil.getNowYYMMDDHHmmss() + "_" + tip + "：" + split[i]);
            }
        }
        return result;
    }
}
