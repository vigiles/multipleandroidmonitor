package com.cuiweiyou.adbctoller;

/**
 * www.gaohaiyan.com
 */
public class Constant {
    private Constant(){}

    /** 资源目录 */
    public static String sourceWorkSpace;

    /** 日志文件 */
    public static String lastLogFilePath;

}
