package com.cuiweiyou.adbctoller.shell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * www.gaohaiyan.com
 */
public class ShellExecuter {

    /**
     * @param shell 指令
     * @param tip   线程-任务标注
     * @param flag  特殊标记  <br/>
     *              22-加好友 <br/>
     *              51-好友通过 <br/>
     *              53-推送话术文件 <br/>
     *              54-聊天 <br/>
     *              55-托送word文件 <br/>
     *              55-托送device文件 <br/>
     *              33-过滤屏幕是否息屏  <br/>
     *              60--从设备pull日志文件  <br/>
     *              44-获取型号、厂商、厂商系统、安卓版本、api版本、微信版本  <br/>
     *              49-获取厂商系统版本，未指定厂商则获取分辨率  <br/>
     *              77-安装微信  <br/>
     *              12-push测试宿主程序  <br/>
     *              13-push测试test程序  <br/>
     *              14-install整个测试程序
     * @return
     */
    public String execCustomShell(String shell, String tip, int flag) {
        String threadName = Thread.currentThread().getName();
        System.out.println("标记：" + tip + " 当前thread：" + threadName);
        BufferedReader bufferedReader = null;
        String result = "";
        try {
            Process process = Runtime.getRuntime().exec(shell);
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while (null != (line = bufferedReader.readLine())) {
                if (!line.isEmpty()) {
                    if (33 == flag) {
                        if (line.contains("Display Power: state=ON")) {
                            System.out.println(tip + "：" + line);
                            sb.append("ON");
                        } else if (line.contains("Display Power: state=OFF")) {
                            System.out.println(tip + "：" + line);
                            sb.append("OFF");
                        }
                    } else if (44 == flag) { // 厂商和型号用换行分割
                        System.out.println(tip + "：" + line);
                        sb.append(line.replace("versionName=", "").trim() + System.getProperty("line.separator"));
                    } else if (49 == flag) { // 厂商的系统版本
                        System.out.println(tip + "：" + line);
                        sb.append(line.replace("Physical size: ", "")); // 如果是分辨率的话
                    } else {
                        System.out.println(tip + "：" + line);
                        sb.append(line);
                    }
                }
            }
            process.waitFor();

            result = sb.toString();

            System.out.println("标记：" + tip + " 执行結果ok：" + result);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("错误：" + tip + " IOException：" + e.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.err.println("错误：" + tip + " InterruptedException：" + e.getMessage());
        } finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }
}
