package com.cuiweiyou.adbctoller.shell;

import com.cuiweiyou.adbctoller.bean.DeviceModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * www.gaohaiyan.com
 */
public class PushFileManager {
    private static PushFileManager instance;
    private Map<DeviceModel, List<String>> pushMap;

    private PushFileManager() {
        pushMap = new HashMap<>();
    }

    public static PushFileManager getInstance() {
        if (null == instance) {
            synchronized (PushFileManager.class) {
                if (null == instance) {
                    instance = new PushFileManager();
                }
            }

        }
        return instance;
    }

    public boolean isPushedThisFile(DeviceModel device, String filePath) {
        boolean isPushed = false;
        List<String> filePaths = pushMap.get(device);
        if (null == filePaths) {
            filePaths = new ArrayList<>();
            pushMap.put(device, filePaths);
        }
        if (!filePaths.contains(filePath)) {
            filePaths.add(filePath);
            isPushed = false;
        } else {
            isPushed = true;
        }

        return isPushed;
    }
}
