package com.cuiweiyou.adbctoller.ui;

import com.cuiweiyou.adbctoller.util.FileUtil;
import com.cuiweiyou.adbctoller.util.LineSeparatorUtil;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

/**
 * www.gaohaiyan.com
 */
public class ChatLoopUI extends JFrame {
    private CtrollerUI parentWindow;
    private JButton stopButton;

    private int frameWidth;
    private int frameHeight;

    private OnStopChatLoopListener onStopChatLoopListener;

    public ChatLoopUI(CtrollerUI frame, OnStopChatLoopListener listener) {
        super("监听聊天");
        FileUtil.saveLogs(LineSeparatorUtil.transformStrigToListLog("创建弹窗", "监听聊天"));

        this.onStopChatLoopListener = listener;

        setAlwaysOnTop(true);
        this.parentWindow = frame;
        this.parentWindow.setUiEnable(false);
        setDefaultLookAndFeelDecorated(true);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        frameWidth = (int) (dimension.getWidth() / 4);
        frameHeight = (int) (dimension.getHeight() / 4);
        setSize(frameWidth, frameHeight);
        int x = (int) (dimension.getWidth() / 2 - frameWidth / 2);
        int y = (int) (dimension.getHeight() / 2 - frameHeight / 2);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setBackground(Color.WHITE);
        setResizable(false);
        setLocation(x, y);
        setSize(frameWidth, frameHeight);

        initView();
        initEvent();

        setVisible(true);
    }

    private void initEvent() {
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FileUtil.saveLogs(LineSeparatorUtil.transformStrigToListLog("关闭监听", "停止按钮"));
                stopChatting();
                dispose();
            }
        });
    }

    @Override
    protected void processWindowEvent(WindowEvent windowEvent) {
        System.out.println("processWindowEvent");


        if (windowEvent.getID() == WindowEvent.WINDOW_CLOSING) {
            FileUtil.saveLogs(LineSeparatorUtil.transformStrigToListLog("关闭监听", "x按钮"));
            stopChatting();
            parentWindow.setUiEnable(true);
            dispose();
        }

        super.processWindowEvent(windowEvent);
    }

    private void initView() {
        int loopImageWidth = frameWidth / 4;
        int loopImageHeight = loopImageWidth;

        int stopButtonWidth = 140;
        int stopButtonHeight = 40;

        int separatorWidth = frameWidth;
        int separatorHeight = frameHeight - loopImageHeight - 40;

        Box verticalBox = Box.createVerticalBox();
        verticalBox.add(getWhiteSeparator(separatorWidth, separatorHeight / 3));
        verticalBox.add(getLoopImage(loopImageWidth, loopImageHeight));
        verticalBox.add(getWhiteSeparator(separatorWidth, separatorHeight / 3));
        verticalBox.add(getStopButton(stopButtonWidth, stopButtonHeight));
        verticalBox.add(getWhiteSeparator(separatorWidth, separatorHeight / 3));
        setContentPane(verticalBox);
    }

    private Component getLoopImage(int panelWidth, int panelHeight) {
        int loadingWH = Math.min(panelWidth, panelHeight) - 20;

        LoadingAnimationUI loadingAnimationUI = new LoadingAnimationUI();
        loadingAnimationUI.setPreferredSize(new Dimension(loadingWH, loadingWH));
        loadingAnimationUI.setBackground(Color.WHITE);

        JPanel imagePanel = new JPanel();
        imagePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        imagePanel.setBackground(Color.WHITE);
        imagePanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        imagePanel.add(loadingAnimationUI);

        return imagePanel;
    }

    private Component getStopButton(int panelWidth, int panelHeight) {
        stopButton = new JButton("停止");

        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        buttonPanel.setBackground(Color.WHITE);
        buttonPanel.add(stopButton);

        return buttonPanel;
    }

    private Component getWhiteSeparator(int separatorWidth, int separatorHeight) {
        JSeparator separator = new JSeparator(SwingConstants.CENTER);
        separator.setPreferredSize(new Dimension(separatorWidth, separatorHeight));
        separator.setBackground(Color.WHITE);
        separator.setForeground(Color.WHITE);

        return separator;
    }

    public void closeWindow() {
        parentWindow.setUiEnable(true);
        dispose();
    }

    private void stopChatting() {
        if (null != onStopChatLoopListener) {
            onStopChatLoopListener.onStopChatLoop();
        }
    }

    public interface OnStopChatLoopListener {
        public void onStopChatLoop();
    }
}
