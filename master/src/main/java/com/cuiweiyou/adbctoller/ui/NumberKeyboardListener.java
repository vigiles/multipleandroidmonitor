package com.cuiweiyou.adbctoller.ui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class NumberKeyboardListener implements KeyListener {

    @Override
    public void keyTyped(KeyEvent e) {
        int temp = e.getKeyChar();
        if (temp == 10) { // 回车
            e.consume();  // 消耗掉
        }
        if (temp == 46) { // 小数点
            e.consume();
        } else {          // 没有按小数点时
            if (temp == 8) { // backspace删除

            } else {
                if (temp > 57) { // asicc码小于0
                    e.consume();
                } else if (temp < 48) { // asicc码大于9
                    e.consume();
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }
}