package com.cuiweiyou.adbctoller.ui;

import com.cuiweiyou.adbctoller.Constant;
import com.cuiweiyou.adbctoller.bean.DeviceModel;
import com.cuiweiyou.adbctoller.bean.WechatListModel;
import com.cuiweiyou.adbctoller.bean.WechatNumberModel;
import com.cuiweiyou.adbctoller.util.FileUtil;
import com.cuiweiyou.adbctoller.util.LineSeparatorUtil;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * www.gaohaiyan.com
 */
public class CtrollerUI extends JFrame {
    int frameWidth;
    int frameHeight;

    /**
     * 要查找添加的微信列表
     */
    List<WechatNumberModel> allWxFriendList;
    /**
     * 选中的手机列表
     */
    List<DeviceModel> selectedDeviceList;
    List<DeviceModel> allDeviceList;

    /**
     * 手机设备的table
     */
    UnEditableTable table;
    /**
     * 手机设备的table的数据管理器
     */
    DefaultTableModel tableModel;
    /**
     * 手机设备的table的显示控制器
     */
    DefaultTableCellRenderer cellRenderer;

    /**
     * 加载的要查找添加的微信列表
     */
    JList<String> friendListView;

    /**
     * 是否全选了手机列表
     */
    boolean isTableSelectAll = false;

    JButton installWxBtn, searchNewFriendBtn, acceptNewFriendBtn, greetingChatBtn, pullDbBtn, pullTxtBtn, pullLogFileBtn, groupChatBtn, flushDevices;
    JButton selectFriendListFileBtn, selectWorkSpaceBtn, selectDevicesBtn, installTestBtn;
    JLabel friendListFileLabel, workSpaceLabel;
    JTextField diffSearchMinTimeTextField, diffSearchMaxTimeTextField, diffChatMinTimeTextField, diffChatMaxTimeTextField;
    JCheckBox loopChatCheckBox, sameFriends, sameGimmicks;
    JScrollPane scrollLogPanel;
    JTextArea logArea;

    OnSelectAllDeviceClickListener onSelectAllDeviceClickListener;
    OnInstallWxAppClickListener onInstallWxAppClickListener;
    OnInstallTestClickListener onInstallTestClickListener;
    OnChooseWXListFileListener onChooseWXListFileListener;
    OnChooseWorkSpaceListener onChooseWorkSpaceListener;
    OnChatGroupClickListener onChatGroupClickListener;
    OnAcceptingClickListener onAcceptingClickListener;
    OnChatingClickListener onChatingClickListener;
    OnSearchClickListener onSearchClickListener;
    OnPullDbClickListener onPullDbClickListener;
    OnFlushDevicesListener onFlushDevicesListener;
    OnPullActionClickListener onPullActionClickListener;

    public CtrollerUI() {
        super("管微Server端");

        setDefaultLookAndFeelDecorated(true);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        frameWidth = (int) (dimension.getWidth() / 4 * 3);
        frameHeight = (int) (dimension.getHeight() / 4 * 3);
        int x = (int) (dimension.getWidth() / 2 - frameWidth / 2);
        int y = (int) (dimension.getHeight() / 2 - frameHeight / 2);

        setLocation(x, y);
        setResizable(false);
        setSize(frameWidth, frameHeight);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        initUI();
        initEvent();

        setVisible(true);
    }

    private void initUI() {
        int topWidth = frameWidth;
        int topHeight = frameHeight / 5 * 3;
        int centerWidth = frameWidth;
        int centerHeight = frameHeight / 5 * 1;
        int bottomWidth = frameWidth;
        int bottomHeight = frameHeight / 5 * 1;

        JPanel contentPpanel = new JPanel();
        contentPpanel.setLayout(new BoxLayout(contentPpanel, BoxLayout.Y_AXIS));
        contentPpanel.add(getTopPanel(topWidth, topHeight));
        contentPpanel.add(getCenterPanel(centerWidth, centerHeight));
        contentPpanel.add(getBottomPanel(bottomWidth, bottomHeight));

        setContentPane(contentPpanel);
    }

    private Component getTopPanel(int panelWidth, int panelHeight) {
        int leftPWidth = panelWidth / 4 * 3;
        int leftPHeight = panelHeight;

        int rightPWidth = panelWidth / 4 * 1;
        int rightPHeight = panelHeight;

        JPanel topPanel = new JPanel();
        topPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        topPanel.setLayout(new BorderLayout());
        topPanel.add(BorderLayout.WEST, getTopLeftPanel(leftPWidth, leftPHeight));
        topPanel.add(BorderLayout.EAST, getTopRightPanel(rightPWidth, rightPHeight));

        return topPanel;
    }

    private Component getTopLeftPanel(int panelWidth, int panelHeight) {

        tableModel = new DefaultTableModel();

        table = new UnEditableTable();
        table.setModel(tableModel);

        table.setFont(new Font("宋体", Font.PLAIN, 12));
        table.getTableHeader().setReorderingAllowed(false);
        table.getTableHeader().setFont(new Font("宋体", Font.BOLD, 16));

        cellRenderer = new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
                if (row % 2 == 0) {
                    setBackground(Color.pink);
                } else if (row % 2 == 1) {
                    setBackground(Color.white);
                }

                int columnCount = table.getColumnCount();
                if (column == 0 || column == (columnCount - 1)) {
                    setHorizontalAlignment(SwingConstants.CENTER);
                } else {
                    setHorizontalAlignment(SwingConstants.LEFT);
                }

                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        };

        table.setRowHeight(table.getRowHeight() + 10);

        JScrollPane tablePanel = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        tablePanel.setPreferredSize(new Dimension(panelWidth, panelHeight));

        return tablePanel;
    }

    private Component getTopRightPanel(int panelWidth, int panelHeight) {
        int separatorWidth = panelWidth;
        int separatorHeight = 5;
        int listWidth = panelWidth;
        int listHeight = panelHeight / 5 * 4;
        int labelWidth = panelWidth;
        int labelHeight = panelHeight - listHeight - separatorHeight;

        friendListView = new JList<>();
        friendListView.setFixedCellHeight(friendListView.getFixedCellHeight() + 30);
        friendListView.setCellRenderer(new DefaultListCellRenderer() {
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.setColor(Color.GRAY);
                g.drawLine(0, getHeight() - 1, getWidth(), getHeight() - 1);
            }
        });

        JScrollPane liistScrollPanel = new JScrollPane(friendListView);
        liistScrollPanel.setPreferredSize(new Dimension(listWidth, listHeight));

        friendListFileLabel = new JLabel();
        JScrollPane labelPanel = new JScrollPane(friendListFileLabel);
        labelPanel.setPreferredSize(new Dimension(labelWidth, labelHeight));

        JPanel topRightPanel = new JPanel();
        topRightPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        topRightPanel.setBackground(Color.WHITE);
        topRightPanel.setLayout(new BoxLayout(topRightPanel, BoxLayout.Y_AXIS));
        topRightPanel.add(liistScrollPanel);
        topRightPanel.add(getSeparator(separatorWidth, separatorHeight));
        topRightPanel.add(labelPanel);

        return topRightPanel;
    }

    private Component getCenterPanel(int panelWidth, int panelHeight) {
        int c1Width = panelWidth;
        int c1Height = panelHeight / 3;
        int c2Width = panelWidth;
        int c2Height = panelHeight / 3;
        int c3Width = panelWidth;
        int c3Height = panelHeight / 3;

        JPanel contentPpanel = new JPanel();
        contentPpanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        contentPpanel.setLayout(new BoxLayout(contentPpanel, BoxLayout.Y_AXIS));
        contentPpanel.add(getCenter1Panel(c1Width, c1Height));
        contentPpanel.add(getCenter2Panel(c2Width, c2Height));
        contentPpanel.add(getCenter3Panel(c3Width, c3Height));

        return contentPpanel;
    }

    private Component getCenter1Panel(int panelWidth, int panelHeight) {
        int leftWidth = panelWidth / 4 * 3;
        int leftHeight = panelHeight;

        int rightWidth = panelWidth / 4 * 1;
        int rightHeight = panelHeight;

        selectWorkSpaceBtn = new JButton("选择资源目录");
        workSpaceLabel = new JLabel();

        JPanel workspacePanel = new JPanel();
        workspacePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        workspacePanel.add(selectWorkSpaceBtn);
        workspacePanel.add(workSpaceLabel);

        flushDevices = new JButton("刷新");

        selectDevicesBtn = new JButton("全选手机");
        JPanel selectdevicesPanel = new JPanel();
        selectdevicesPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        selectdevicesPanel.add(flushDevices);
        selectdevicesPanel.add(selectDevicesBtn);

        selectFriendListFileBtn = new JButton("加载名单");
        JPanel rightPanel = new JPanel();
        rightPanel.setPreferredSize(new Dimension(rightWidth, rightHeight));
        rightPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        rightPanel.add(selectFriendListFileBtn);

        JPanel centerLeftPanel = new JPanel();
        centerLeftPanel.setPreferredSize(new Dimension(leftWidth, leftHeight));
        centerLeftPanel.setLayout(new BorderLayout());
        centerLeftPanel.add(BorderLayout.WEST, workspacePanel);
        centerLeftPanel.add(BorderLayout.EAST, selectdevicesPanel);

        JPanel centerPanel1 = new JPanel();
        centerPanel1.setPreferredSize(new Dimension(panelWidth, panelHeight));
        centerPanel1.setLayout(new BorderLayout());
        centerPanel1.add(BorderLayout.WEST, centerLeftPanel);
        centerPanel1.add(BorderLayout.EAST, rightPanel);

        return centerPanel1;
    }

    private Component getCenter2Panel(int panelWidth, int panelHeight) {
        getGroupChatPanel(panelWidth, panelHeight);
        //chatPanel.add(getGroupChatBtn); // todo 拉群

        int installWidth = panelWidth / 5 * 2;
        int installHeight = panelHeight;

        int chatWidth = panelWidth / 5 * 3;
        int chatHeight = panelHeight;

        JPanel centerPanel2 = new JPanel();
        centerPanel2.setPreferredSize(new Dimension(panelWidth, panelHeight));
        centerPanel2.setLayout(new BorderLayout());
        centerPanel2.add(BorderLayout.WEST, getInstallPanel(installWidth, installHeight));
        centerPanel2.add(BorderLayout.EAST, getChatPanel(chatWidth, chatHeight));

        return centerPanel2;
    }

    private Component getInstallPanel(int panelWidth, int panelHeight) {
        installWxBtn = new JButton("安装微信");
        installTestBtn = new JButton("安装Test");

        JPanel installAppsPanel = new JPanel();
        //installAppsPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        installAppsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        installAppsPanel.add(installWxBtn);
        installAppsPanel.add(installTestBtn);

        return installAppsPanel;
    }

    private Component getChatPanel(int panelWidth, int panelHeight) {
        int searchWidth = panelWidth / 2;
        int searchHeight = panelHeight;
        int loopWidth = panelWidth / 2;
        int loopHeight = panelHeight;

        JPanel chatPanel = new JPanel();
        //chatPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        chatPanel.setLayout(new BorderLayout());
        chatPanel.add(BorderLayout.WEST, getSearchPanel(searchWidth, searchHeight));
        chatPanel.add(BorderLayout.EAST, getLoopChatPanel(loopWidth, loopHeight));

        return chatPanel;
    }

    private Component getGroupChatPanel(int panelWidth, int panelHeight) {
        groupChatBtn = new JButton("拉群聊天");
        return null;
    }

    private Component getSearchPanel(int panelWidth, int panelHeight) {

        sameFriends = new JCheckBox("一致");
        sameFriends.setSelected(true);

        JLabel diffTimeLabel = new JLabel("间隔秒:");
        searchNewFriendBtn = new JButton("添加朋友");
        diffSearchMinTimeTextField = new JTextField("10");
        diffSearchMinTimeTextField.setPreferredSize(new Dimension(50, 30));
        diffSearchMinTimeTextField.addKeyListener(new NumberKeyboardListener());

        JLabel pTimeLabel = new JLabel("-");

        diffSearchMaxTimeTextField = new JTextField("30");
        diffSearchMaxTimeTextField.setPreferredSize(new Dimension(50, 30));
        diffSearchMaxTimeTextField.addKeyListener(new NumberKeyboardListener());

        JPanel searchPanel = new JPanel();
        //searchPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        searchPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        searchPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        searchPanel.add(sameFriends);
        searchPanel.add(diffTimeLabel);
        searchPanel.add(diffSearchMinTimeTextField);
        searchPanel.add(pTimeLabel);
        searchPanel.add(diffSearchMaxTimeTextField);
        searchPanel.add(searchNewFriendBtn);

        return searchPanel;
    }

    private Component getLoopChatPanel(int panelWidth, int panelHeight) {

        greetingChatBtn = new JButton("回复消息");

        sameGimmicks = new JCheckBox("一致");
        sameGimmicks.setSelected(true);

        loopChatCheckBox = new JCheckBox("保持监听");
        loopChatCheckBox.setSelected(false);

        JLabel diffChatTimeLabel = new JLabel("间隔秒:");
        diffChatMinTimeTextField = new JTextField("10");
        diffChatMinTimeTextField.setPreferredSize(new Dimension(50, 30));
        diffChatMinTimeTextField.addKeyListener(new NumberKeyboardListener());

        JLabel pTimeLabel = new JLabel("-");

        diffChatMaxTimeTextField = new JTextField("30");
        diffChatMaxTimeTextField.setPreferredSize(new Dimension(50, 30));
        diffChatMaxTimeTextField.addKeyListener(new NumberKeyboardListener());

        JPanel loopPanel = new JPanel();
        //loopPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        loopPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        loopPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        loopPanel.add(sameGimmicks);
        loopPanel.add(diffChatTimeLabel);
        loopPanel.add(diffChatMinTimeTextField);
        loopPanel.add(pTimeLabel);
        loopPanel.add(diffChatMaxTimeTextField);
        loopPanel.add(loopChatCheckBox);
        loopPanel.add(greetingChatBtn);

        return loopPanel;
    }

    private Component getCenter3Panel(int panelWidth, int panelHeight) {
        acceptNewFriendBtn = new JButton("陌生人请求通过");
        JPanel acceptPanel = new JPanel();
        acceptPanel.add(acceptNewFriendBtn);

        pullDbBtn = new JButton("提取DB操作日志");
        pullTxtBtn = new JButton("提取TXT操作日志");
        pullLogFileBtn = new JButton("提取动作日志");

        JPanel pullDbPanel = new JPanel();
        pullDbPanel.add(pullLogFileBtn);
        pullDbPanel.add(pullTxtBtn);
        pullDbPanel.add(pullDbBtn);

        JPanel centerPanel3 = new JPanel();
        centerPanel3.setPreferredSize(new Dimension(panelWidth, panelHeight));
        centerPanel3.setLayout(new BorderLayout());
        //centerPanel3.add(BorderLayout.WEST, acceptPanel);
        centerPanel3.add(BorderLayout.EAST, pullDbPanel);

        return centerPanel3;
    }

    private Component getBottomPanel(int panelWidth, int panelHeight) {
        logArea = new JTextArea();

        scrollLogPanel = new JScrollPane(logArea);
        scrollLogPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        return scrollLogPanel;
    }

    private Component getSeparator(int panelWidth, int panelHeight) {
        JSeparator separator = new JSeparator(SwingConstants.CENTER);
        separator.setPreferredSize(new Dimension(panelHeight, panelHeight));
        separator.setBackground(Color.WHITE);
        separator.setForeground(Color.WHITE);
        return separator;
    }

    private void scrollPanelToBottom(JScrollPane scrollPanel) {
        JScrollBar scrollBar = scrollPanel.getVerticalScrollBar();
        scrollPanel.getViewport().setViewPosition(new Point(0, scrollBar.getMaximum()));
    }

    private void initEvent() {

        // 添加标格监听事件
        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    int columnIndex = table.columnAtPoint(e.getPoint()); // 获取点击的列
                    int rowIndex = table.rowAtPoint(e.getPoint()); // 获取点击的行
                    int columnCount = table.getColumnCount();
                    int index = columnCount - 1;
                    String isSelectedValue = table.getValueAt(rowIndex, index).toString();
                    System.out.println("点击的行：" + rowIndex + "，列：" + columnIndex + "，选中：" + isSelectedValue);
                    saveLogs(LineSeparatorUtil.transformStrigToListLog("点击的行：", rowIndex + "，列：" + columnIndex + "，选中：" + isSelectedValue));

                    // "编号", "手机", "型号", "厂商", "系统", "Android", "API", "微信", "选择"
                    String address = table.getValueAt(rowIndex, 0).toString();
                    String sn = table.getValueAt(rowIndex, 1).toString();
                    String mdl = table.getValueAt(rowIndex, 2).toString();
                    String brand = table.getValueAt(rowIndex, 3).toString();
                    String bsv = table.getValueAt(rowIndex, 4).toString();
                    String av = table.getValueAt(rowIndex, 5).toString();
                    String api = table.getValueAt(rowIndex, 6).toString();
                    String wx = table.getValueAt(rowIndex, 7).toString();
                    DeviceModel device = new DeviceModel(sn, brand, mdl, bsv, av, api, wx);

                    if (null == selectedDeviceList) {
                        selectedDeviceList = new ArrayList<>();
                    }

                    if ("x".equals(isSelectedValue)) { // 未选中时
                        table.setValueAt("yyyyy", rowIndex, index); // 点击后，选中
                        if (!selectedDeviceList.contains(device)) {
                            selectedDeviceList.add(device);
                        }
                    } else {// 已经选中
                        table.setValueAt("x", rowIndex, index);
                        if (selectedDeviceList.contains(device)) {
                            selectedDeviceList.remove(device);
                        }
                    }

                    System.out.println("电机设备：" + selectedDeviceList.size());
                }
            }
        });

        selectFriendListFileBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String fileListFilePath = null;
                if (null != Constant.sourceWorkSpace && !"".equals(Constant.sourceWorkSpace)) {
                    File file = new File(Constant.sourceWorkSpace + "friend.txt");
                    if (file.exists()) {
                        fileListFilePath = file.getAbsolutePath();
                    }
                } else {
                    showToast("请选择资源目录");
                    return;
                }

                if (null == fileListFilePath || "".equals(fileListFilePath)) {
                    File file222 = chooseAFile("friend", "txt", false, "选择friend.txt文件");
                    if (null != file222) {
                        fileListFilePath = file222.getAbsolutePath();
                    } else {
                        showToast("请选择friend.txt文件");
                        return;
                    }
                }

                friendListFileLabel.setText("<html>名单文件：" + fileListFilePath + "</html>");
                if (null != onChooseWXListFileListener) {
                    onChooseWXListFileListener.onChoosedWXListFile(fileListFilePath);
                }
            }
        });

        flushDevices.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (null != onFlushDevicesListener) {
                    onFlushDevicesListener.onFlushDevices();
                }
            }
        });

        selectDevicesBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TableModel model = table.getModel();
                int rowCount = model.getRowCount();
                int columnCount = model.getColumnCount();

                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (isTableSelectAll) {
                    isTableSelectAll = false;
                    table.clearSelection();

                    for (int i = 0; i < rowCount; i++) {
                        table.setValueAt("x", i, columnCount - 1);

                        // "编号", "手机", "型号", "厂商", "系统", "Android", "API", "微信", "选择"
                        String address = table.getValueAt(i, 0).toString();
                        String sn = table.getValueAt(i, 1).toString();
                        String mdl = table.getValueAt(i, 2).toString();
                        String brand = table.getValueAt(i, 3).toString();
                        String bsv = table.getValueAt(i, 4).toString();
                        String av = table.getValueAt(i, 5).toString();
                        String api = table.getValueAt(i, 6).toString();
                        String wx = table.getValueAt(i, 7).toString();
                        DeviceModel device = new DeviceModel(sn, brand, mdl, bsv, av, api, wx);

                        if (selectedDeviceList.contains(device)) {
                            selectedDeviceList.remove(device);
                        }
                    }
                } else {
                    isTableSelectAll = true;
                    table.selectAll();

                    for (int i = 0; i < rowCount; i++) {
                        table.setValueAt("yyyyy", i, columnCount - 1);

                        // "编号", "手机", "型号", "厂商", "系统", "Android", "API", "微信", "选择"
                        String address = table.getValueAt(i, 0).toString();
                        String sn = table.getValueAt(i, 1).toString();
                        String mdl = table.getValueAt(i, 2).toString();
                        String brand = table.getValueAt(i, 3).toString();
                        String bsv = table.getValueAt(i, 4).toString();
                        String av = table.getValueAt(i, 5).toString();
                        String api = table.getValueAt(i, 6).toString();
                        String wx = table.getValueAt(i, 7).toString();
                        DeviceModel device = new DeviceModel(sn, brand, mdl, bsv, av, api, wx);

                        if (!selectedDeviceList.contains(device)) {
                            selectedDeviceList.add(device);
                        }
                    }
                }

                int[] rows = table.getSelectedRows();
                for (int i = 0; i < rows.length; i++) {
                    int row = rows[i];
                    System.out.println("=======" + row);
                }

                //if (selectedDeviceList.size() < 1) {
                //    showToast("未选择设备");
                //    return;
                //}

                if (null != onSelectAllDeviceClickListener) {
                    onSelectAllDeviceClickListener.onSelectAllDeviceClick(selectedDeviceList);
                }
            }
        });

        selectWorkSpaceBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(false);
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setDialogTitle("选择资源文件夹");
                int result = chooser.showOpenDialog(CtrollerUI.this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File file = chooser.getSelectedFile();
                    if (null != file) {
                        String filepath = file.getAbsolutePath() + "/";
                        workSpaceLabel.setText(filepath);
                        if (null != onChooseWorkSpaceListener) {
                            onChooseWorkSpaceListener.onChoosedWorkSpace(filepath, allDeviceList);
                        }
                    } else {
                        showToast("请选择资源文件夹");
                    }
                } else {
                    showToast("请选择资源文件夹");
                }
            }
        });

        installWxBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String wxFilePath = null;
                if (null != Constant.sourceWorkSpace && !"".equals(Constant.sourceWorkSpace)) {
                    File file = new File(Constant.sourceWorkSpace + "com.tencent.mm_1570e359_6.7.3.apk");
                    if (file.exists()) {
                        wxFilePath = new File(Constant.sourceWorkSpace + "com.tencent.mm_1570e359_6.7.3.apk").getAbsolutePath();
                    }
                } else {
                    showToast("请选择资源目录");
                    return;
                }

                if (null == wxFilePath || "".equals(wxFilePath)) {
                    File file222 = chooseAFile("com.tencent.mm_1570e359_6.7.3", "apk", false, "选择com.tencent.mm_1570e359_6.7.3.apk文件");
                    if (null != file222) {
                        wxFilePath = file222.getAbsolutePath();
                    } else {
                        showToast("请选择com.tencent.mm_1570e359_6.7.3.apk文件");
                        return;
                    }
                }

                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (null != onInstallWxAppClickListener) {

                    onInstallWxAppClickListener.onInstallWxAppClick(selectedDeviceList, wxFilePath);
                }
            }
        });

        installTestBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String app_debug_androidTest_Path = null;
                String app_debug_Path = null;
                if (null != Constant.sourceWorkSpace && !"".equals(Constant.sourceWorkSpace)) {
                    File file = new File(Constant.sourceWorkSpace + "app-debug-androidTest.apk");
                    if (file.exists()) {
                        app_debug_androidTest_Path = file.getAbsolutePath();
                    }

                    file = new File(Constant.sourceWorkSpace + "app-debug.apk");
                    if (file.exists()) {
                        app_debug_Path = file.getAbsolutePath();
                    }
                } else {
                    showToast("请选择资源目录");
                    return;
                }

                if (null == app_debug_androidTest_Path || "".equals(app_debug_androidTest_Path)) {
                    File file222 = chooseAFile("app-debug-androidTest", "apk", false, "选择app-debug-androidTest.apk文件");
                    if (null != file222) {
                        app_debug_androidTest_Path = file222.getAbsolutePath();
                    } else {
                        showToast("请选择app-debug-androidTest.apk文件");
                        return;
                    }
                }

                if (null == app_debug_Path || "".equals(app_debug_Path)) {
                    File file222 = chooseAFile("app-debug", "apk", false, "选择app-debug.apk文件");
                    if (null != file222) {
                        app_debug_Path = file222.getAbsolutePath();
                    } else {
                        showToast("请选择app-debug.apk文件");
                        return;
                    }
                }


                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (null != onInstallTestClickListener) {

                    onInstallTestClickListener.onInstallTestClick(selectedDeviceList, app_debug_androidTest_Path, app_debug_Path);
                }
            }
        });

        searchNewFriendBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (null == Constant.sourceWorkSpace || "".equals(Constant.sourceWorkSpace)) {
                    showToast("请选择资源目录");
                    return;
                }

                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }
                if (null == allWxFriendList) {
                    allWxFriendList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (allWxFriendList.size() < 1) {
                    showToast("未加载好友名单");
                    return;
                }

                boolean allWxVersionOk = isAllWxVersionOk();
                if (!allWxVersionOk) {
                    return;
                }

                int diffMinTime = 1;
                String textMin = diffSearchMinTimeTextField.getText();
                if (null != textMin && !"".equals(textMin.trim())) {
                    try {
                        Integer integer = Integer.valueOf(textMin.trim());
                        if (integer > 0) {
                            diffMinTime = integer;
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                }

                int diffMaxTime = 1;
                String textMax = diffSearchMaxTimeTextField.getText();
                if (null != textMax && !"".equals(textMax.trim())) {
                    try {
                        Integer integer = Integer.valueOf(textMax.trim());
                        if (integer > 0) {
                            diffMaxTime = integer;
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                }

                if (diffMinTime > diffMaxTime) {
                    diffMaxTime = diffMinTime;
                }

                String hello = JOptionPane.showInputDialog("请输入打招呼语句(或留空)：");
                if (null == hello) {
                    hello = "a88a";
                } else {
                    hello = hello.trim();
                    if (hello.length() < 1) {
                        hello = "a88a";
                    }
                }

                String deviceFilePath = Constant.sourceWorkSpace + "device.txt";
                File deviceFile = new File(deviceFilePath);
                if (!deviceFile.exists()) {
                    File file222 = chooseAFile("device", "txt", false, "选择设备匹配文件device.txt");
                    if (null != file222) {
                        deviceFilePath = file222.getAbsolutePath();
                    } else {
                        showToast("请选择设备匹配文件device.txt");
                        return;
                    }
                }

                if (null != onSearchClickListener) {
                    onSearchClickListener.onSearchClick(selectedDeviceList, allWxFriendList, diffMinTime, diffMaxTime, hello, deviceFilePath);
                }
            }
        });

        acceptNewFriendBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (null != onAcceptingClickListener) {

                    onAcceptingClickListener.onAcceptingClick(selectedDeviceList);
                }
            }
        });

        pullDbBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (null == Constant.sourceWorkSpace || "".equals(Constant.sourceWorkSpace)) {
                    showToast("请选择资源目录");
                    return;
                }

                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (null != onPullDbClickListener) {

                    onPullDbClickListener.onPullDbClick(selectedDeviceList, 0);
                }
            }
        });

        pullLogFileBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (null == Constant.sourceWorkSpace || "".equals(Constant.sourceWorkSpace)) {
                    showToast("请选择资源目录");
                    return;
                }

                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (null != onPullActionClickListener) {
                    onPullActionClickListener.onPullActionClick(selectedDeviceList);
                }
            }
        });

        pullTxtBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (null == Constant.sourceWorkSpace || "".equals(Constant.sourceWorkSpace)) {
                    showToast("请选择资源目录");
                    return;
                }

                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (null != onPullDbClickListener) {
                    onPullDbClickListener.onPullDbClick(selectedDeviceList, 1);
                }
            }
        });

        greetingChatBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (null == Constant.sourceWorkSpace || "".equals(Constant.sourceWorkSpace)) {
                    showToast("请选择资源目录");
                    return;
                }

                boolean allWxVersionOk = isAllWxVersionOk();
                if (!allWxVersionOk) {
                    return;
                }

                String gimmickCommFilePath = Constant.sourceWorkSpace + "gimmick_comm.txt";
                File gimmickCommFile = new File(gimmickCommFilePath);
                if (!gimmickCommFile.exists() || gimmickCommFile.length() < 50) {
                    File file222 = chooseAFile("gimmick_comm", "txt", false, "选择话术文件gimmick_comm.txt");
                    if (null != file222) {
                        gimmickCommFilePath = file222.getAbsolutePath();
                    } else {
                        showToast("请选择话术文件gimmick_comm.txt");
                        return;
                    }
                }

                String gimmickHelloFilePath = Constant.sourceWorkSpace + "gimmick_hello.txt";
                File gimmickHelloFile = new File(gimmickHelloFilePath);
                if (!gimmickHelloFile.exists() || gimmickHelloFile.length() < 100) {
                    File file222 = chooseAFile("gimmick_hello", "txt", false, "选择话术文件gimmick_hello.txt");
                    if (null != file222) {
                        gimmickHelloFilePath = file222.getAbsolutePath();
                    } else {
                        showToast("请选择话术文件gimmick_hello.txt");
                        return;
                    }
                }

                boolean selectedSameGimmicks = sameGimmicks.isSelected();
                if (!selectedSameGimmicks) {
                    for (int i = 0; i < selectedDeviceList.size(); i++) {
                        DeviceModel device = selectedDeviceList.get(i);
                        String sn = device.getSn();
                        String gimmickFilePath = Constant.sourceWorkSpace + "gimmick_hello_" + sn.toLowerCase() + ".txt";
                        File file = new File(gimmickFilePath);
                        if (!file.exists() || file.length() < 50) {
                            showToast("手机没有匹配话术文件gimmick_hello_" + sn.toLowerCase() + ".txt");
                            saveLog("手机没有匹配话术文件gimmick_hello_" + sn.toLowerCase() + ".txt");
                            return;
                        }
                    }

                    for (int i = 0; i < selectedDeviceList.size(); i++) {
                        DeviceModel device = selectedDeviceList.get(i);
                        String sn = device.getSn();
                        String gimmickFilePath = Constant.sourceWorkSpace + "gimmick_comm_" + sn.toLowerCase() + ".txt";
                        File file = new File(gimmickFilePath);
                        if (!file.exists() || file.length() < 100) {
                            showToast("手机没有匹配话术文件gimmick_comm_" + sn.toLowerCase() + ".txt");
                            saveLog("手机没有匹配话术文件gimmick_comm_" + sn.toLowerCase() + ".txt");
                            return;
                        }
                    }
                }

                String wordFilePath = Constant.sourceWorkSpace + "word.txt";
                File wordFile = new File(wordFilePath);
                if (!wordFile.exists()) {
                    File file222 = chooseAFile("word", "txt", false, "选择分词文件word.txt");
                    if (null != file222) {
                        wordFilePath = file222.getAbsolutePath();
                    } else {
                        showToast("请选择分词文件word.txt");
                        return;
                    }
                }

                String deviceFilePath = Constant.sourceWorkSpace + "device.txt";
                File deviceFile = new File(deviceFilePath);
                if (!deviceFile.exists()) {
                    File file222 = chooseAFile("device", "txt", false, "选择设备匹配文件device.txt");
                    if (null != file222) {
                        deviceFilePath = file222.getAbsolutePath();
                    } else {
                        showToast("请选择设备匹配文件device.txt");
                        return;
                    }
                }

                int diffMinTime = 1;
                String textMin = diffChatMinTimeTextField.getText();
                if (null != textMin && !"".equals(textMin.trim())) {
                    try {
                        Integer integer = Integer.valueOf(textMin.trim());
                        if (integer > 0) {
                            diffMinTime = integer;
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                }

                int diffMaxTime = 1;
                String textMax = diffChatMaxTimeTextField.getText();
                if (null != textMax && !"".equals(textMax.trim())) {
                    try {
                        Integer integer = Integer.valueOf(textMax.trim());
                        if (integer > 0) {
                            diffMaxTime = integer;
                        }
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                }

                if (diffMinTime > diffMaxTime) {
                    diffMaxTime = diffMinTime + 10;
                }


                if (null != onChatingClickListener) {
                    onChatingClickListener.onChattingClick(selectedDeviceList, gimmickCommFilePath, gimmickHelloFilePath, wordFilePath, deviceFilePath, diffMinTime, diffMaxTime);
                }
            }
        });

        groupChatBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                if (null == selectedDeviceList) {
                    selectedDeviceList = new ArrayList<>();
                }

                if (selectedDeviceList.size() < 1) {
                    showToast("未选择设备");
                    return;
                }

                if (allWxFriendList.size() < 1) {
                    showToast("未加载好友名单");
                    return;
                }

                if (null != onChatGroupClickListener) {
                    onChatGroupClickListener.onChatGroupClick(selectedDeviceList);
                }
            }
        });
    }

    public boolean isKeep() {
        return loopChatCheckBox.isSelected();
    }

    public boolean isSameGimmicks() {
        return sameGimmicks.isSelected();
    }

    public boolean isSameFriends() {
        return sameFriends.isSelected();
    }

    public boolean stopKeep() {
        loopChatCheckBox.setSelected(false);
        return isKeep();
    }

    public void loadFriendList() {
        File file = new File(Constant.sourceWorkSpace + "friend.txt");
        saveLogs(LineSeparatorUtil.transformStrigToListLog("资源目录中的微信列表文件：", file.getAbsolutePath()));
        if (file.exists()) {
            String path = file.getAbsolutePath();
            friendListFileLabel.setText("<html>名单文件：" + path + "</html>");
            if (null != onChooseWXListFileListener) {
                onChooseWXListFileListener.onChoosedWXListFile(path); // 重新加载要查找的微信列表
            }
        }
    }

    private File chooseAFile(String fileName, String type, boolean muliteChooseAble, String title) {
        final String targetName = fileName;
        final String targetType = type;
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(muliteChooseAble);
        fileChooser.setDialogTitle(title);
        fileChooser.setFileFilter(new FileFilter() {
            public String getDescription() {
                return targetName + "." + targetType;
            }

            public boolean accept(File file) {
                String name = file.getName();
                return name.equals(targetName + "." + targetType);
            }
        });
        fileChooser.addChoosableFileFilter(new FileFilter() {
            public String getDescription() {
                return "*." + targetType;
            }

            public boolean accept(File file) {
                String name = file.getName();
                saveLogs(LineSeparatorUtil.transformStrigToListLog("选择的文件：", name));
                return name.toLowerCase().endsWith(targetType);
            }
        });
        fileChooser.showOpenDialog(CtrollerUI.this);// 显示打开的文件对话框
        File file222 = fileChooser.getSelectedFile();
        return file222;
    }

    public void showToast(String msg) {
        JOptionPane.showMessageDialog(null, msg);
        saveLogs(LineSeparatorUtil.transformStrigToListLog("提示：", msg));
    }

    public void setData(List<DeviceModel> list) {
        String[] heads = {"编号", "手机", "型号", "厂商", "系统", "Android", "API", "微信", "选择"};

        if (null == list || list.size() < 1) {
            if (null != allDeviceList) {
                allDeviceList.clear();
            }
            if (null != selectedDeviceList) {
                selectedDeviceList.clear();
            }

            String[][] deviceData = new String[0][6];
            tableModel.setDataVector(deviceData, heads);
            tableModel.fireTableDataChanged();
            table.updateUI();

            return;
        }

        allDeviceList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            allDeviceList.add(list.get(i));
        }


        int size = list.size();

        String[][] deviceData = new String[size][6];
        for (int i = 0; i < size; i++) {
            DeviceModel bean = list.get(i);
            String sn = bean.getSn();
            String model = bean.getModel();
            String brand = bean.getBrand();
            String bsv = bean.getBrandSystemVersion();
            String av = bean.getAndroidVersion();
            String api = bean.getApiVersion();
            String wx = bean.getWxVersion();
            if ("null".equals(wx)) {
                wx = "未安装";
            }
            String[] device = {i + "", sn, model, brand, bsv, av, api, wx, "x"};
            deviceData[i] = device;
        }

        tableModel.setDataVector(deviceData, heads);

        int columnCount = table.getColumnCount();
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(columnCount - 1).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        for (int i = 0; i < columnCount; i++) {
            TableColumn column = columnModel.getColumn(i);
            if (0 == i || i == columnCount - 1) {
                column.setPreferredWidth(60);
            } else {
                column.setPreferredWidth((frameWidth - 120) / (columnCount - 1));
            }

            column.setCellRenderer(cellRenderer);
        }

        table.updateUI();

        isAllWxVersionOk();
    }

    /**
     * 保存要查找的微信列表
     *
     * @param list
     */
    public void setAllWxFriendList(List<WechatNumberModel> list) {
        this.allWxFriendList = list;

        List<String> wxidList = new ArrayList<>();
        for (int i = 0; i < allWxFriendList.size(); i++) {
            WechatNumberModel friend = allWxFriendList.get(i);
            String wxid = friend.getWxid();
            wxidList.add(wxid);
        }

        friendListView.setModel(new WechatListModel(wxidList));
    }

    public void saveLog(String log) {
        saveLogs(new ArrayList<String>() {{
            add(log);
        }});
    }

    public void saveLogs(List<String> logList) {
        if (null != logList && logList.size() > 0) {
            FileUtil.saveLogs(logList);
            for (int i = 0; i < logList.size(); i++) {
                logArea.append(logList.get(i) + System.getProperty("line.separator"));
            }
            scrollPanelToBottom(scrollLogPanel);
        }
    }

    private boolean isAllWxVersionOk() {
        if (null != allDeviceList && allDeviceList.size() > 0) {
            for (int i = 0; i < allDeviceList.size(); i++) {
                DeviceModel model = allDeviceList.get(i);
                String wxVersion = model.getWxVersion();
                if (!"6.7.3".equals(wxVersion)) {
                    showToast("有不匹配的微信版本，请手动重新安装6.7.3版");
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public void setUiEnable(boolean enable) {
        selectWorkSpaceBtn.setEnabled(enable);
        selectDevicesBtn.setEnabled(enable);
        selectFriendListFileBtn.setEnabled(enable);
        installTestBtn.setEnabled(enable);
        installWxBtn.setEnabled(enable);
        diffSearchMinTimeTextField.setEnabled(enable);
        diffSearchMaxTimeTextField.setEnabled(enable);
        diffChatMinTimeTextField.setEnabled(enable);
        diffChatMaxTimeTextField.setEnabled(enable);
        searchNewFriendBtn.setEnabled(enable);
        loopChatCheckBox.setEnabled(enable);
        greetingChatBtn.setEnabled(enable);
        acceptNewFriendBtn.setEnabled(enable);
        pullTxtBtn.setEnabled(enable);
        pullDbBtn.setEnabled(enable);
        sameFriends.setEnabled(enable);
        sameGimmicks.setEnabled(enable);
    }

    public CtrollerUI setOnSelectAllDeviceClickListener(OnSelectAllDeviceClickListener onSelectAllDeviceClickListener) {
        this.onSelectAllDeviceClickListener = onSelectAllDeviceClickListener;
        return this;
    }

    public CtrollerUI setOnSearchClickListener(OnSearchClickListener onSearchClickListener) {
        this.onSearchClickListener = onSearchClickListener;
        return this;
    }

    public void setOnAcceptingClickListener(OnAcceptingClickListener onAcceptingClickListener) {
        this.onAcceptingClickListener = onAcceptingClickListener;
    }

    public CtrollerUI setOnChooseWXListFileListener(OnChooseWXListFileListener onChooseWXListFileListener) {
        this.onChooseWXListFileListener = onChooseWXListFileListener;
        return this;
    }

    public CtrollerUI setOnInstallWxAppClickListener(OnInstallWxAppClickListener onInstallAppClickListener) {
        this.onInstallWxAppClickListener = onInstallAppClickListener;
        return this;
    }

    public CtrollerUI setOnInstallTestClickListener(OnInstallTestClickListener onInstallTestClickListener) {
        this.onInstallTestClickListener = onInstallTestClickListener;
        return this;
    }

    public CtrollerUI setOnChatGroupClickListener(OnChatGroupClickListener onChatGroupClickListener) {
        this.onChatGroupClickListener = onChatGroupClickListener;
        return this;
    }

    public CtrollerUI setOnChatingClickListener(OnChatingClickListener listener) {
        this.onChatingClickListener = listener;
        return this;
    }

    public void setOnPullActionClickListener(OnPullActionClickListener listener) {
        this.onPullActionClickListener = listener;
    }

    public void setOnPullDbClickListener(OnPullDbClickListener onPullDbClickListener) {
        this.onPullDbClickListener = onPullDbClickListener;
    }

    public void setOnChooseWorkSpaceListener(OnChooseWorkSpaceListener listener) {
        this.onChooseWorkSpaceListener = listener;
    }

    public void setOnFlushDevicesListener(OnFlushDevicesListener listener) {
        this.onFlushDevicesListener = listener;
    }

    /**
     * 选择资源目录
     */
    public static interface OnChooseWorkSpaceListener {
        /**
         * 选择资源目录
         *
         * @param path 目录
         */
        void onChoosedWorkSpace(String path, List<DeviceModel> allDevices);
    }

    /**
     * 加载微信列表文件
     */
    public static interface OnChooseWXListFileListener {
        /**
         * 加载微信列表
         *
         * @param filePath
         */
        void onChoosedWXListFile(String filePath);
    }

    /**
     * 选择手机设备
     */
    public static interface OnSelectAllDeviceClickListener {
        /**
         * 选择了手机
         *
         * @param list
         */
        void onSelectAllDeviceClick(List<DeviceModel> list);
    }

    /**
     * 安装微信
     */
    public static interface OnInstallWxAppClickListener {
        /**
         * 安装微信
         *
         * @param list       手机设备
         * @param wxFilePath 微信apk路径
         */
        void onInstallWxAppClick(List<DeviceModel> list, String wxFilePath);
    }

    /**
     * 安装测试apk
     */
    public static interface OnInstallTestClickListener {
        /**
         * 安装测试apk，宿主apk和测试器apk
         *
         * @param list                       手机设备
         * @param app_debug_androidTest_Path 测试器apk
         * @param app_debug_Path             宿主apk
         */
        void onInstallTestClick(List<DeviceModel> list, String app_debug_androidTest_Path, String app_debug_Path);
    }

    /**
     * 查找添加好友
     */
    public static interface OnSearchClickListener {
        /**
         * 查找添加新朋友
         *
         * @param dlist       手机设备
         * @param fList       新微信号
         * @param diffMinTime 两次查找间隔最小时长
         * @param diffMaxTime 两次查找间隔最大时长
         * @param hello       打招呼语句
         */
        void onSearchClick(List<DeviceModel> dlist, List<WechatNumberModel> fList, int diffMinTime, int diffMaxTime, String hello, String deviceFilePath);
    }

    /**
     * 同意陌生微信号的主动添加
     */
    public static interface OnAcceptingClickListener {
        /**
         * 同意陌生微信号的主动添加
         *
         * @param deviceList 手机
         */
        void onAcceptingClick(List<DeviceModel> deviceList);
    }

    /**
     * 拉取设备中的操作日志
     */
    public static interface OnPullActionClickListener {
        /**
         * @param deviceList
         */
        void onPullActionClick(List<DeviceModel> deviceList);
    }

    /**
     * 拉取设备中的操作日志
     */
    public static interface OnPullDbClickListener {
        /**
         * @param deviceList
         * @param flag       0-db文件，1-txt文件
         */
        void onPullDbClick(List<DeviceModel> deviceList, int flag);
    }

    /**
     * 遍历聊天
     */
    public static interface OnChatingClickListener {
        /**
         * 遍历聊天
         *
         * @param deviceList           设备
         * @param gimmickCommFilePath  话术文件，话术制作参看：<a href="https://gitee.com/vigiles/gimmickmaker">https://gitee.com/vigiles/gimmickmaker</a>
         * @param gimmickHelloFilePath 话术文件
         * @param wordFilePath         分词文件
         */
        void onChattingClick(List<DeviceModel> deviceList, String gimmickCommFilePath, String gimmickHelloFilePath, String wordFilePath, String deviceFilePath, int diffMinTime, int diffMaxTime);
    }

    /**
     * 拉群聊天
     */
    public static interface OnChatGroupClickListener {
        /**
         * 拉群聊天
         *
         * @param list 设备
         */
        void onChatGroupClick(List<DeviceModel> list);
    }

    public static interface OnFlushDevicesListener {

        void onFlushDevices();
    }

}
