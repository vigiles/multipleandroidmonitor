package com.cuiweiyou.adbctoller.ui;

import com.cuiweiyou.adbctoller.util.FileUtil;
import com.cuiweiyou.adbctoller.util.LineSeparatorUtil;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * www.gaohaiyan.com
 */
public class SearchingUI extends JFrame {
    private OnStopSearchingListener stopChatLoopListener;

    private CtrollerUI parentWindow;
    private JButton stopButton;

    private int frameWidth;
    private int frameHeight;

    public SearchingUI(String title, CtrollerUI frame, OnStopSearchingListener listener) {
        super(title);
        FileUtil.saveLogs(LineSeparatorUtil.transformStrigToListLog("创建弹窗", title));
        this.stopChatLoopListener = listener;

        setAlwaysOnTop(true);
        this.parentWindow = frame;
        this.parentWindow.setUiEnable(false);
        setDefaultLookAndFeelDecorated(true);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        frameWidth = (int) (dimension.getWidth() / 4);
        frameHeight = (int) (dimension.getHeight() / 4);
        setSize(frameWidth, frameHeight);
        int x = (int) (dimension.getWidth() / 2 - frameWidth / 2);
        int y = (int) (dimension.getHeight() / 2 - frameHeight / 2);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setBackground(Color.WHITE);
        setResizable(false);
        setLocation(x, y);
        setSize(frameWidth, frameHeight);

        initView();
        initEvent();

        setVisible(true);
    }

    @Override
    protected void processWindowEvent(WindowEvent windowEvent) {
        System.out.println("processWindowEvent");

        if (windowEvent.getID() == WindowEvent.WINDOW_CLOSING) {
            FileUtil.saveLogs(LineSeparatorUtil.transformStrigToListLog("关闭弹窗", "x按钮"));
            stopChatting();
            parentWindow.setUiEnable(true);
            dispose();
        }

        super.processWindowEvent(windowEvent);
    }

    private void initView() {
        int loadingWH = frameWidth / 4;
        int stopWidth = frameWidth;
        int stopHeight = 60;
        int glueWidth = frameHeight;
        int glueHeight = (frameHeight - loadingWH - stopHeight) / 4;

        Box verticalStrut = Box.createVerticalBox();
        verticalStrut.setPreferredSize(new Dimension(frameWidth, frameHeight));
        verticalStrut.add(getVerticalGlue(glueWidth, glueHeight));
        verticalStrut.add(getLoadingImage(loadingWH, loadingWH));
        verticalStrut.add(getVerticalGlue(glueWidth, glueHeight));
        verticalStrut.add(getStopButton(stopWidth, stopHeight));
        verticalStrut.add(getVerticalGlue(glueWidth, glueHeight));

        setContentPane(verticalStrut);
    }

    private Component getVerticalGlue(int panelWidth, int panelHeight) {
        Component verticalGlue2 = Box.createVerticalGlue();
        verticalGlue2.setPreferredSize(new Dimension(panelWidth, panelHeight));
        return verticalGlue2;
    }

    private Component getLoadingImage(int panelWidth, int panelHeight) {

        LoadingAnimationUI loadingAnimationUI = new LoadingAnimationUI();
        loadingAnimationUI.setPreferredSize(new Dimension(panelWidth, panelHeight));
        loadingAnimationUI.setBackground(Color.WHITE);

        JPanel imagePanel = new JPanel();
        imagePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        imagePanel.setBackground(Color.WHITE);
        imagePanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        imagePanel.add(loadingAnimationUI);

        return imagePanel;
    }

    private Component getStopButton(int panelWidth, int panelHeight) {
        stopButton = new JButton("停止");

        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        buttonPanel.setBackground(Color.WHITE);
        buttonPanel.add(stopButton);

        return buttonPanel;
    }

    private void initEvent() {
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FileUtil.saveLogs(LineSeparatorUtil.transformStrigToListLog("关闭弹窗", "停止按钮"));
                stopChatting();
                dispose();
            }
        });
    }

    private void stopChatting() {
        if (null != stopChatLoopListener) {
            stopChatLoopListener.onStopSearching();
        }
    }

    public interface OnStopSearchingListener {
        public void onStopSearching();
    }
}
