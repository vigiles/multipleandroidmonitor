package com.cuiweiyou.adbctoller.ui;

import javax.swing.JTable;

/**
 * www.gaohaiyan.com
 */
public class UnEditableTable extends JTable {

	public UnEditableTable(Object[][] deviceData, String[] heads) {
		super(deviceData, heads);
	}

	public UnEditableTable() {
	}

	public boolean isCellEditable(int row, int column) {// 表格不可编辑
		return false;
	}
}
