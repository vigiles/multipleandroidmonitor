package com.cuiweiyou.adbctoller.ui;

import com.cuiweiyou.adbctoller.util.FileUtil;
import com.cuiweiyou.adbctoller.util.LineSeparatorUtil;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * www.gaohaiyan.com
 */
public class LoadingUI extends JFrame {
    private CtrollerUI parentWindow;

    private int frameWidth;
    private int frameHeight;

    public LoadingUI(String title, CtrollerUI frame) {
        super(title);
        FileUtil.saveLogs(LineSeparatorUtil.transformStrigToListLog("创建弹窗", title));

        setAlwaysOnTop(true);
        this.parentWindow = frame;
        this.parentWindow.setUiEnable(false);
        setDefaultLookAndFeelDecorated(true);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        frameWidth = (int) (dimension.getWidth() / 4);
        frameHeight = (int) (dimension.getHeight() / 4);
        setSize(frameWidth, frameHeight);
        int x = (int) (dimension.getWidth() / 2 - frameWidth / 2);
        int y = (int) (dimension.getHeight() / 2 - frameHeight / 2);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setBackground(Color.WHITE);
        setResizable(false);
        setLocation(x, y);
        setSize(frameWidth, frameHeight);

        initView();

        setVisible(true);
    }

    @Override
    protected void processWindowEvent(WindowEvent windowEvent) {
        System.out.println("processWindowEvent");

        if (windowEvent.getID() == WindowEvent.WINDOW_CLOSING) {

        }

        super.processWindowEvent(windowEvent);
    }

    private void initView() {
        int loadingWH = frameWidth / 4;
        int glueHeight = (frameHeight - loadingWH) / 3;

        LoadingAnimationUI loadingAnimationUI = new LoadingAnimationUI();
        loadingAnimationUI.setPreferredSize(new Dimension(loadingWH, loadingWH));
        loadingAnimationUI.setBackground(Color.WHITE);

        JPanel imagePanel = new JPanel();
        imagePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        imagePanel.setBackground(Color.WHITE);
        imagePanel.setPreferredSize(new Dimension(frameWidth, loadingWH));
        imagePanel.add(loadingAnimationUI);

        Component verticalGlue1 = Box.createVerticalGlue();
        verticalGlue1.setPreferredSize(new Dimension(frameWidth, glueHeight));
        Component verticalGlue2 = Box.createVerticalGlue();
        verticalGlue2.setPreferredSize(new Dimension(frameWidth, glueHeight));

        Box verticalStrut = Box.createVerticalBox();
        verticalStrut.setPreferredSize(new Dimension(frameWidth, frameHeight));
        verticalStrut.add(verticalGlue1);
        verticalStrut.add(imagePanel);
        verticalStrut.add(verticalGlue2);

        setContentPane(verticalStrut);
    }

}
