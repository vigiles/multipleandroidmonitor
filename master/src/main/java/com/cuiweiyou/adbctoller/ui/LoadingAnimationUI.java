package com.cuiweiyou.adbctoller.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * www.gaohaiyan.com
 */
// 来自csdn上某位博主...。
public class LoadingAnimationUI extends JPanel {
    private int delay;
    private int arcAngle = 0;
    private int startAngle;
    private int orientation; //0 顺时针，1逆时针

    private Timer timer;

    public LoadingAnimationUI() {
        this.delay = 50;
        this.orientation = 0;

        this.timer = new Timer(delay, new ReboundListener());
        this.timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g.create();
        //抗锯齿
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int width = getWidth();
        int height = getHeight();
        int min = Math.min(width, height);
        int diffX = 0;
        int diffY = 0;
        if (width > height) {
            diffX = (width - height) / 2;
        }
        if (width < height) {
            diffY = (height - width) / 2;
        }
        int lineWith = 10;
        //设置画笔颜色
        g2d.setColor(Color.WHITE);
        g2d.drawArc(0 + diffX, 0 + diffY, min - diffX, min - diffY, 0, 360);
        g2d.setColor(Color.GREEN);
        g2d.fillArc(0 + diffX, 0 + diffY, min - diffX, min - diffY, startAngle, arcAngle);
        g2d.setColor(Color.WHITE);
        g2d.fillArc(0 + diffX + lineWith, 0 + diffY + lineWith, min - diffX - lineWith * 2, min - diffY - lineWith * 2, 0, 360);
        g2d.dispose();
    }

    private class ReboundListener implements ActionListener {

        private int o = 0;

        @Override
        public void actionPerformed(ActionEvent e) {
            if (startAngle < 360) {
                //控制每个DELAY周期旋转的角度，+ 为逆时针  - 为顺时针
                switch (orientation) {
                    case 0:
                        startAngle = startAngle + 5;
                        break;
                    case 1:
                        startAngle = startAngle - 5;
                        break;
                    default:
                        startAngle = startAngle + 5;
                        break;
                }
            } else {
                startAngle = 0;
            }

            if (o == 0) {
                if (arcAngle >= 355) {
                    o = 1;
                    orientation = 1;
                } else {
                    if (orientation == 0) {
                        arcAngle += 5;
                    }
                }
            } else {
                if (arcAngle <= 5) {
                    o = 0;
                    orientation = 0;
                } else {
                    if (orientation == 1) {
                        arcAngle -= 5;
                    }
                }
            }

            repaint();
        }
    }
}